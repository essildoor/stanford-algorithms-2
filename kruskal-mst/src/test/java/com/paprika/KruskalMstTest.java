package com.paprika;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Created by andrey.kapitonov on 04.04.2015.
 */
public class KruskalMstTest {
    private static final String resourcesPath = "src/test/resources/";
    private KruskalMst kruskalMst;

    @Before
    public void setUp() throws Exception {
        this.kruskalMst = new KruskalMst();
    }

    @Test
    public void testOne() throws Exception {
        Assert.assertEquals(6d, kruskalMst.findMstCost(Util.readFromFile(resourcesPath + "test1.txt")), 0);
    }

    @Test
    public void testTwo() throws Exception {
        Assert.assertEquals(4d, kruskalMst.findMstCost(Util.readFromFile(resourcesPath + "test2.txt")), 0);
    }

    @Test
    public void testThree() throws Exception {
        Assert.assertEquals(-16d, kruskalMst.findMstCost(Util.readFromFile(resourcesPath + "test3.txt")), 0);
    }

    @Test
    public void testFour() throws Exception {
        Assert.assertEquals(1.81d, kruskalMst.findMstCost(Util.readFromFile(resourcesPath + "test4.txt")), 0.001d);
    }

    @Test
    public void testLarge() throws Exception {
        long startTime = System.currentTimeMillis();
        List<Edge> graph = Util.readFromFile(resourcesPath + "largeEWG.txt");
        long readTime = System.currentTimeMillis() - startTime;
        System.out.println("reading time: " + readTime / 1000f + " sec");
        startTime = System.currentTimeMillis();
        double answer = kruskalMst.findMstCost(graph);
        long answTime = System.currentTimeMillis() - startTime;
        Assert.assertEquals(647.663d, answer, 0.001d);
        System.out.println("running time: " + answTime / 1000f + " sec");
    }
}
