package com.paprika;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by essildoor on 3/29/15.
 */
public class Vertex {

    private static Map<Integer, Vertex> objectCache = new HashMap<>();

    public static Vertex getInstance(int number) {
        if (!objectCache.keySet().contains(number)) {
            final Vertex v = new Vertex(number);
            objectCache.put(number, v);
            return v;
        }
        return objectCache.get(number);
    }

    private int number;
    private Vertex leader;

    private Vertex(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public Vertex getLeader() {
        return leader;
    }

    public void setLeader(Vertex leader) {
        this.leader = leader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(number, vertex.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
