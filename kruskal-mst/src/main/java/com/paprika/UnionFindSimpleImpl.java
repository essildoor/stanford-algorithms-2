package com.paprika;

import java.util.*;

/**
 * Created by andrey.kapitonov on 04.04.2015.
 */
public class UnionFindSimpleImpl implements UnionFind {

    private Map<Vertex, LinkedList<Vertex>> storage;

    public UnionFindSimpleImpl() {
        this.storage = new HashMap<>();
    }

    public void makeSet(Vertex v) {
        final LinkedList<Vertex> newList = new LinkedList<>();
        v.setLeader(v);
        newList.add(v);
        storage.put(v, newList);
    }

    /**
     * @param v vertex
     * @return head of the set which contains specified vertex
     */
    public Vertex find(Vertex v) {
        return v.getLeader();
    }

    public void union(Vertex first, Vertex second) {
        final LinkedList<Vertex> firstList = storage.get(first);
        final LinkedList<Vertex> secondList = storage.get(second);
        if (firstList.size() > secondList.size()) {
            changeLeader(secondList, first);
            firstList.addAll(secondList);
            storage.remove(second);
        } else {
            changeLeader(firstList, second);
            secondList.addAll(firstList);
            storage.remove(first);
        }
    }

    private void changeLeader(LinkedList<Vertex> list, Vertex newLeader) {
        for (Vertex e : list) {
            e.setLeader(newLeader);
        }
    }
}
