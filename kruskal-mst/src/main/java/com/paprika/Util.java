package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by andrey.kapitonov on 04.04.2015.
 */
public class Util {
    public static List<Edge> readFromFile(String fileName) throws IOException {
        List<String> lines = null;

        try {
            lines = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (lines == null || lines.size() <= 1) return new ArrayList<>();
        StringTokenizer st0 = new StringTokenizer(lines.get(0), " ");
        final int numVertices = Integer.parseInt(st0.nextToken());
        final int numEdges = Integer.parseInt(st0.nextToken());
        lines = lines.subList(1, lines.size());
        final List<Edge> graph = new ArrayList<>(lines.size());
        for (String line : lines) {
            final StringTokenizer st = new StringTokenizer(line, " ");
            final int currentVertexNumber = Integer.parseInt(st.nextToken());
            final int targetVertexNumber = Integer.parseInt(st.nextToken());
            final float edgeWeight = Float.parseFloat(st.nextToken());
            graph.add(new Edge(edgeWeight, Vertex.getInstance(currentVertexNumber), Vertex.getInstance(targetVertexNumber)));
        }
        Collections.sort(graph);
        return graph;
    }
}
