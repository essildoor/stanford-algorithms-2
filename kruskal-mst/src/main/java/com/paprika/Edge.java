package com.paprika;

/**
 * Created by andrey.kapitonov on 04.04.2015.
 */
public class Edge implements Comparable<Edge> {
    private float weight;
    private Vertex first;
    private Vertex second;

    public Edge(float weight, Vertex first, Vertex second) {
        this.weight = weight;
        this.first = first;
        this.second = second;
    }

    public float getWeight() {
        return weight;
    }

    public Vertex getFirst() {
        return first;
    }

    public Vertex getSecond() {
        return second;
    }

    @Override
    public int compareTo(Edge o) {
        if (this.weight == o.weight) return 0;
        return this.weight > o.weight ? 1 : -1;
    }
}
