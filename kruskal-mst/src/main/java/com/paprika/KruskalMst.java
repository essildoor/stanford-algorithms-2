package com.paprika;

import java.util.List;

/**
 *
 * Created by andrey.kapitonov on 04.04.2015.
 */
public class KruskalMst {

    public double findMstCost(List<Edge> graph) {
        final UnionFind uf = new UnionFindSimpleImpl();
        double result = 0d;
        for (Edge edge : graph) {
            final Vertex firstLeader = uf.find(edge.getFirst());
            final Vertex secondLeader = uf.find(edge.getSecond());
            if (firstLeader == null && secondLeader == null) {
                uf.makeSet(edge.getFirst());
                uf.makeSet(edge.getSecond());
                uf.union(edge.getFirst(), edge.getSecond());
                result += edge.getWeight();
            } else if (firstLeader == null) {
                uf.makeSet(edge.getFirst());
                uf.union(edge.getFirst(), secondLeader);
                result += edge.getWeight();
            } else if (secondLeader == null) {
                uf.makeSet(edge.getSecond());
                uf.union(edge.getSecond(), firstLeader);
                result += edge.getWeight();
            } else if (!firstLeader.equals(secondLeader)) {
                uf.union(firstLeader, secondLeader);
                result += edge.getWeight();
            }
        }
        return result;
    }
}
