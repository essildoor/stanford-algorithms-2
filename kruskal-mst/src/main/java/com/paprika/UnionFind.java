package com.paprika;

import java.util.Objects;

/**
 *
 * Created by andrey.kapitonov on 04.04.2015.
 */
public interface UnionFind {

    void makeSet(Vertex v);

    Vertex find(Vertex v);

    void union(Vertex first, Vertex second);
}
