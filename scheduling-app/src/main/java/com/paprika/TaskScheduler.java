package com.paprika;

import java.util.*;

/**
 * Created by essildoor on 3/27/15.
 */
public class TaskScheduler {

    class ComparatorOne implements Comparator<Task> {
        public int compare(Task o1, Task o2) {
            Integer diff1 = o1.getWeight() - o1.getLength();
            Integer diff2 = o2.getWeight() - o2.getLength();
            if (!diff1.equals(diff2)) return diff2.compareTo(diff1);
            return o2.getWeight() - o1.getWeight();
        }
    }

    class ComparatorTwo implements Comparator<Task> {
        public int compare(Task o1, Task o2) {
            float diff = (o1.getWeight() / (float) o1.getLength()) - (o2.getWeight() / (float) o2.getLength());
            return diff >= 0 ? -1 : 1;
        }
    }

    public long doSchedule(List<Task> tasks, Comparator<Task> comparator) {
        Collections.sort(tasks, comparator);
        int lengthSum = 0;
        long result = 0;
        for (Task t : tasks) {
            lengthSum += t.getLength();
            t.setLength(lengthSum);
            result += t.getWeight() * t.getLength();
        }
        return result;
    }
}
