package com.paprika;

public class Task {
    private int weight;
    private int length;

    public Task(int weight, int length) {
        this.length = length;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}