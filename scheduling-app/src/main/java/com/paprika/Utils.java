package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by essildoor on 3/27/15.
 */
public class Utils {
    public static List<Task> readFromFile(String fileName) {
        List<String> lines = null;

        try {
            lines = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (lines == null) return null;
        List<Task> result = new ArrayList<>();
        lines = lines.subList(1, lines.size());
        for (String line : lines) {
            StringTokenizer st = new StringTokenizer(line, " ");
            Task task = new Task(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
            result.add(task);
        }

        return result;
    }
}
