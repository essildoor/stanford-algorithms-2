package com.paprika;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Comparator;

/**
 * Created by essildoor on 3/27/15.
 */
public class TaskSchedulerTest {
    private TaskScheduler scheduler;
    private Comparator<Task> comparator1;
    private Comparator<Task> comparator2;


    @Before
    public void setUp() throws Exception {
        scheduler = new TaskScheduler();
        comparator1 = scheduler.new ComparatorOne();
        comparator2 = scheduler.new ComparatorTwo();
    }

    @Test
    public void testOne() throws Exception {
        assertEquals(31814, scheduler.doSchedule(Utils.readFromFile("test1.txt"), comparator1));
        assertEquals(31814, scheduler.doSchedule(Utils.readFromFile("test1.txt"), comparator2));
    }

    @Test
    public void testTwo() throws Exception {
        assertEquals(61545, scheduler.doSchedule(Utils.readFromFile("test2.txt"), comparator1));
        assertEquals(60213, scheduler.doSchedule(Utils.readFromFile("test2.txt"), comparator2));
    }

    @Test
    public void testThree() throws Exception {
        assertEquals(688647, scheduler.doSchedule(Utils.readFromFile("test3.txt"), comparator1));
        assertEquals(674634, scheduler.doSchedule(Utils.readFromFile("test3.txt"), comparator2));
    }

    @Test
    public void testMain() throws Exception {
        System.out.println(scheduler.doSchedule(Utils.readFromFile("jobs.txt"), comparator1));
        System.out.println(scheduler.doSchedule(Utils.readFromFile("jobs.txt"), comparator2));
    }
}
