package com.paprika;

import java.util.*;

/**
 *
 * Created by essildoor on 3/29/15.
 */
public class PrimMst {

    public double findMstCost(final Map<Integer, Vertex> graph) {
        //initial step
        final Set<Integer> spanned = new HashSet<>(graph.size());
        final PriorityQueue<Vertex> rest = new PriorityQueue<>(graph.values());
        int spannedVertex = randomInt(1, graph.size());
        double mstCost = 0d;
        spanned.add(spannedVertex);
        rest.remove(graph.get(spannedVertex));

        //main part
        while (!rest.isEmpty()) {
            for (int targetVertexNum : graph.get(spannedVertex).getAdjacencyMap().keySet()) {
                if (!spanned.contains(targetVertexNum)) {
                    final Vertex v = graph.get(targetVertexNum);
                    rest.remove(v);
                    v.setKey(recalculateKey(v, spanned));
                    rest.add(v);
                }
            }

            mstCost += rest.peek().getKey();
            spannedVertex = rest.poll().getNumber();
            spanned.add(spannedVertex);
        }

        return mstCost;
    }

    private float recalculateKey(Vertex v, Set<Integer> spanned) {
        SortedSet<Float> tmp = new TreeSet<>();
        for (Integer targetVertex : v.getAdjacencyMap().keySet()) {
            if (spanned.contains(targetVertex)) {
                tmp.add(v.getAdjacencyMap().get(targetVertex));
            }
        }
        return tmp.first();
    }

    private int randomInt(final int min, final int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
