package com.paprika;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by essildoor on 3/29/15.
 */
public class Vertex implements Comparable<Vertex> {
    private int number;
    private Map<Integer, Float> adjacencyMap;
    private float key;

    public Vertex(int number) {
        this.number = number;
        this.adjacencyMap = new HashMap<>();
        this.key = Integer.MAX_VALUE;
    }

    public int getNumber() {
        return number;
    }

    public Map<Integer, Float> getAdjacencyMap() {
        return adjacencyMap;
    }

    public float getKey() {
        return key;
    }

    public void setKey(float key) {
        this.key = key;
    }


    @Override
    public int compareTo(Vertex o) {
        if (this.key != o.key) return this.key - o.key > 0 ? 1 : -1;
        return this.number - o.number;
    }

    @Override
    public String toString() {
        return "adjMap: " + adjacencyMap.toString() + ", key: " + key;
    }
}
