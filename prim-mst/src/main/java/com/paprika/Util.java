package com.paprika;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 *
 * Created by essildoor on 3/29/15.
 */
public class Util {
    public static Map<Integer, Vertex> readFromFile(String fileName) throws IOException {
        long startTime = System.currentTimeMillis();
        List<String> lines = null;

        try {
            lines = Files.readAllLines(FileSystems.getDefault().getPath(fileName), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        long fileRead = System.currentTimeMillis() - startTime;
        System.out.println("file read in " + fileRead / 1000f + " sec");

        startTime = System.currentTimeMillis();
        if (lines == null || lines.size() <= 1) return new HashMap<>();
        StringTokenizer st0 = new StringTokenizer(lines.get(0), " ");
        final int numVertices = Integer.parseInt(st0.nextToken());
        final int numEdges = Integer.parseInt(st0.nextToken());
        lines = lines.subList(1, lines.size());
        final Map<Integer, Vertex> graph = new HashMap<>(numVertices);

        long initTime = System.currentTimeMillis() - startTime;
        System.out.println("init done in " + initTime / 1000f + " sec");
        startTime = System.currentTimeMillis();

        int counter = 0;
        for (String line : lines) {
            final StringTokenizer st = new StringTokenizer(line, " ");
            final int currentVertexNumber = Integer.parseInt(st.nextToken());
            final int targetVertexNumber = Integer.parseInt(st.nextToken());
            final float edgeWeight = Float.parseFloat(st.nextToken());
            final Vertex currentVertex;
            final Vertex targetVertex;
            if (graph.get(currentVertexNumber) == null) {
                currentVertex = new Vertex(currentVertexNumber);
                graph.put(currentVertexNumber, currentVertex);
            } else {
                currentVertex = graph.get(currentVertexNumber);
            }
            currentVertex.getAdjacencyMap().put(targetVertexNumber, edgeWeight);
            if (graph.get(targetVertexNumber) == null) {
                targetVertex = new Vertex(targetVertexNumber);
                graph.put(targetVertexNumber, targetVertex);
            } else {
                targetVertex = graph.get(targetVertexNumber);
            }
            targetVertex.getAdjacencyMap().put(currentVertexNumber, edgeWeight);
            if (counter >= 10000 && counter % 10000 == 0) System.out.println(counter + " lines processed");
        }
        long constructionTime = System.currentTimeMillis() - startTime;
        System.out.println("graph constructed in " + constructionTime / 1000f + " sec");
        return graph;
    }
}
