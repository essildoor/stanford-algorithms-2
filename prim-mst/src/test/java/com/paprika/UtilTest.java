package com.paprika;

import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by essildoor on 3/29/15.
 */
public class UtilTest {
    private static final String resourcesPath = "src/test/resources/";

    @Test
    public void testCorrect() throws Exception {
        Map<Integer, Vertex> graph = Util.readFromFile(resourcesPath + "test1.txt");
        assertNotNull(graph);
    }

    @Test(expected = IOException.class)
    public void testIncorrectVertices() throws Exception {
        Map<Integer, Vertex> graph = Util.readFromFile(resourcesPath + "testIncorrectInput1.txt");
    }

    @Test(expected = IOException.class)
    public void testIncorrectEdges() throws Exception {
        Map<Integer, Vertex> graph = Util.readFromFile(resourcesPath + "testIncorrectInput2.txt");
    }
}
