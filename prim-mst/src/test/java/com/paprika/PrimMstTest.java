package com.paprika;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by essildoor on 3/29/15.
 */
public class PrimMstTest {
    private static final String resourcesPath = "src/test/resources/";

    private PrimMst primMst;

    @Before
    public void setUp() throws Exception {
        primMst = new PrimMst();
    }

    @Test
    public void testOne() throws Exception {
        assertEquals(6d, primMst.findMstCost(Util.readFromFile(resourcesPath + "test1.txt")), 0);
    }

    @Test
    public void testTwo() throws Exception {
        assertEquals(4d, primMst.findMstCost(Util.readFromFile(resourcesPath + "test2.txt")), 0);
    }

    @Test
    public void testThree() throws Exception {
        assertEquals(-16d, primMst.findMstCost(Util.readFromFile(resourcesPath + "test3.txt")), 0);
    }

    @Test
    public void testFour() throws Exception {
        assertEquals(1.81d, primMst.findMstCost(Util.readFromFile(resourcesPath + "test4.txt")), 0.001d);
    }

    @Test
    public void testMain() throws Exception {
        System.out.println((int) primMst.findMstCost(Util.readFromFile(resourcesPath + "edges.txt")));
    }
}
