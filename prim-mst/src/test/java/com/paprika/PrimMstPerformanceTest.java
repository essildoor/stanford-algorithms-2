package com.paprika;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by essildoor on 3/30/15.
 */
public class PrimMstPerformanceTest {
    private static final String resourcesPath = "src/test/resources/";

    private PrimMst primMst;

    @Before
    public void setUp() throws Exception {
        primMst = new PrimMst();
    }

    @Test
    public void testLarge() throws Exception {
        long startTime = System.currentTimeMillis();
        Map<Integer, Vertex> graph = Util.readFromFile(resourcesPath + "largeEWG.txt");
        long readTime = System.currentTimeMillis() - startTime;

        //System.out.println("reading time: " + readTime / 1000f + " sec");

        startTime = System.currentTimeMillis();
        double answer = primMst.findMstCost(graph);
        long runningTime = System.currentTimeMillis() - startTime;

        Assert.assertEquals(647.663d, answer, 0.001d);
        System.out.println("running time: " + runningTime / 1000f + " sec");
        System.out.println("full time: " + ((readTime + runningTime) / 1000f) + " sec");
    }
}
